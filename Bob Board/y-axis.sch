EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 6400 3600 0    50   Input ~ 0
SDI
Text HLabel 6400 3700 0    50   Input ~ 0
SDO
Text HLabel 6400 3500 0    50   Input ~ 0
SCK
$Comp
L TMC:TMC5160_BOB U?
U 1 1 6105840F
P 7300 4000
AR Path="/60FF5A28/6105840F" Ref="U?"  Part="1" 
AR Path="/60FF6C5A/6105840F" Ref="U1"  Part="1" 
F 0 "U1" H 7300 4981 50  0000 C CNN
F 1 "TMC5160_BOB" H 7300 4890 50  0000 C CNB
F 2 "TMC:DIP-22_1400_ELL" H 6200 5700 50  0001 C CNN
F 3 "" H 6200 5700 50  0001 C CNN
	1    7300 4000
	1    0    0    -1  
$EndComp
Text HLabel 6400 3400 0    50   Input ~ 0
CSN
Wire Wire Line
	6400 3400 6600 3400
Wire Wire Line
	6400 3500 6600 3500
Wire Wire Line
	6400 3600 6600 3600
Wire Wire Line
	6400 3700 6600 3700
$Comp
L power:GND #PWR?
U 1 1 6105841A
P 7100 4800
AR Path="/6105841A" Ref="#PWR?"  Part="1" 
AR Path="/60FF5A28/6105841A" Ref="#PWR?"  Part="1" 
AR Path="/60FF6C5A/6105841A" Ref="#PWR02"  Part="1" 
F 0 "#PWR02" H 7100 4550 50  0001 C CNN
F 1 "GND" H 7105 4627 50  0000 C CNN
F 2 "" H 7100 4800 50  0001 C CNN
F 3 "" H 7100 4800 50  0001 C CNN
	1    7100 4800
	1    0    0    -1  
$EndComp
$Comp
L power:Vdrive #PWR?
U 1 1 61058420
P 7500 2900
AR Path="/61058420" Ref="#PWR?"  Part="1" 
AR Path="/60FF5A28/61058420" Ref="#PWR?"  Part="1" 
AR Path="/60FF6C5A/61058420" Ref="#PWR03"  Part="1" 
F 0 "#PWR03" H 7300 2750 50  0001 C CNN
F 1 "Vdrive" H 7515 3073 50  0000 C CNN
F 2 "" H 7500 2900 50  0001 C CNN
F 3 "" H 7500 2900 50  0001 C CNN
	1    7500 2900
	1    0    0    -1  
$EndComp
$Comp
L power:+5P #PWR?
U 1 1 61058426
P 7100 2900
AR Path="/61058426" Ref="#PWR?"  Part="1" 
AR Path="/60FF5A28/61058426" Ref="#PWR?"  Part="1" 
AR Path="/60FF6C5A/61058426" Ref="#PWR01"  Part="1" 
F 0 "#PWR01" H 7100 2750 50  0001 C CNN
F 1 "+5P" H 7115 3073 50  0000 C CNN
F 2 "" H 7100 2900 50  0001 C CNN
F 3 "" H 7100 2900 50  0001 C CNN
	1    7100 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 4800 7400 4800
Connection ~ 7100 4800
Connection ~ 7200 4800
Wire Wire Line
	7200 4800 7100 4800
Connection ~ 7400 4800
Wire Wire Line
	7400 4800 7200 4800
Wire Wire Line
	7100 3200 7100 2900
Wire Wire Line
	7500 2900 7500 3200
$Comp
L Connector:Screw_Terminal_01x04 J?
U 1 1 61058434
P 8200 3500
AR Path="/60FF5A28/61058434" Ref="J?"  Part="1" 
AR Path="/60FF6C5A/61058434" Ref="J3"  Part="1" 
F 0 "J3" H 8280 3492 50  0000 L CNN
F 1 "Screw_Terminal_01x04" H 8280 3401 50  0000 L CNN
F 2 "TerminalBlock:TerminalBlock_bornier-4_P5.08mm" H 8200 3500 50  0001 C CNN
F 3 "~" H 8200 3500 50  0001 C CNN
	1    8200 3500
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J?
U 1 1 61061B83
P 2900 3550
AR Path="/60FF5A28/61061B83" Ref="J?"  Part="1" 
AR Path="/60FF6C5A/61061B83" Ref="J6"  Part="1" 
F 0 "J6" H 2818 3225 50  0000 C CNN
F 1 "Screw_Terminal_01x02" H 2818 3316 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 2900 3550 50  0001 C CNN
F 3 "~" H 2900 3550 50  0001 C CNN
	1    2900 3550
	-1   0    0    1   
$EndComp
Text HLabel 6300 4000 0    50   Input ~ 0
DRV_ENN
Wire Wire Line
	6300 4000 6600 4000
Wire Wire Line
	6600 3900 6500 3900
Wire Wire Line
	6500 3900 6500 4800
Wire Wire Line
	6500 4800 7100 4800
Text HLabel 8400 4600 2    50   Output ~ 0
DIAG
Wire Wire Line
	8000 4600 8400 4600
$Comp
L Isolator:PC827 U?
U 1 1 610A1418
P 4550 3550
AR Path="/60FF5A28/610A1418" Ref="U?"  Part="1" 
AR Path="/60FF6C5A/610A1418" Ref="U5"  Part="1" 
F 0 "U5" H 4550 3875 50  0000 C CNN
F 1 "PC827" H 4550 3784 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 4350 3350 50  0001 L CIN
F 3 "http://optoelectronics.liteon.com/upload/download/DS-70-96-0016/LTV-8X7%20series.PDF" H 4550 3550 50  0001 L CNN
	1    4550 3550
	1    0    0    -1  
$EndComp
$Comp
L Isolator:PC827 U?
U 2 1 610A141E
P 4550 4100
AR Path="/60FF5A28/610A141E" Ref="U?"  Part="2" 
AR Path="/60FF6C5A/610A141E" Ref="U5"  Part="2" 
F 0 "U5" H 4550 4425 50  0000 C CNN
F 1 "PC827" H 4550 4334 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 4350 3900 50  0001 L CIN
F 3 "http://optoelectronics.liteon.com/upload/download/DS-70-96-0016/LTV-8X7%20series.PDF" H 4550 4100 50  0001 L CNN
	2    4550 4100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 610A1424
P 5000 4650
AR Path="/60FF5A28/610A1424" Ref="R?"  Part="1" 
AR Path="/60FF6C5A/610A1424" Ref="R8"  Part="1" 
F 0 "R8" H 5070 4696 50  0000 L CNN
F 1 "R" H 5070 4605 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Vertical" V 4930 4650 50  0001 C CNN
F 3 "~" H 5000 4650 50  0001 C CNN
	1    5000 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 610A142A
P 5400 4650
AR Path="/60FF5A28/610A142A" Ref="R?"  Part="1" 
AR Path="/60FF6C5A/610A142A" Ref="R9"  Part="1" 
F 0 "R9" H 5470 4696 50  0000 L CNN
F 1 "R" H 5470 4605 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Vertical" V 5330 4650 50  0001 C CNN
F 3 "~" H 5400 4650 50  0001 C CNN
	1    5400 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 4100 5550 4100
Wire Wire Line
	5550 4100 5550 3650
$Comp
L power:+5V #PWR?
U 1 1 610A1432
P 4900 3350
AR Path="/60FF5A28/610A1432" Ref="#PWR?"  Part="1" 
AR Path="/60FF6C5A/610A1432" Ref="#PWR0115"  Part="1" 
F 0 "#PWR0115" H 4900 3200 50  0001 C CNN
F 1 "+5V" H 4915 3523 50  0000 C CNN
F 2 "" H 4900 3350 50  0001 C CNN
F 3 "" H 4900 3350 50  0001 C CNN
	1    4900 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 3350 4900 3450
Wire Wire Line
	4900 4000 4850 4000
Wire Wire Line
	4850 3450 4900 3450
Connection ~ 4900 3450
Wire Wire Line
	4900 3450 4900 4000
Wire Wire Line
	4850 4200 5400 4200
Wire Wire Line
	4850 3650 5000 3650
Wire Wire Line
	5000 4800 5400 4800
Connection ~ 5400 4800
Wire Wire Line
	5000 4500 5000 3650
Connection ~ 5000 3650
Wire Wire Line
	5000 3650 5550 3650
Wire Wire Line
	5400 4500 5400 4200
Connection ~ 5400 4200
Wire Wire Line
	5400 4200 6600 4200
$Comp
L Device:R R?
U 1 1 610A1448
P 3850 3450
AR Path="/60FF5A28/610A1448" Ref="R?"  Part="1" 
AR Path="/60FF6C5A/610A1448" Ref="R6"  Part="1" 
F 0 "R6" V 3643 3450 50  0000 C CNN
F 1 "R" V 3734 3450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Vertical" V 3780 3450 50  0001 C CNN
F 3 "~" H 3850 3450 50  0001 C CNN
	1    3850 3450
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 610A144E
P 3850 4000
AR Path="/60FF5A28/610A144E" Ref="R?"  Part="1" 
AR Path="/60FF6C5A/610A144E" Ref="R7"  Part="1" 
F 0 "R7" V 3643 4000 50  0000 C CNN
F 1 "R" V 3734 4000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Vertical" V 3780 4000 50  0001 C CNN
F 3 "~" H 3850 4000 50  0001 C CNN
	1    3850 4000
	0    1    1    0   
$EndComp
Wire Wire Line
	3100 3450 3700 3450
Wire Wire Line
	4000 3450 4250 3450
Wire Wire Line
	3100 3550 3550 3550
Wire Wire Line
	3550 3550 3550 4000
Wire Wire Line
	3550 4000 3700 4000
Wire Wire Line
	4000 4000 4250 4000
Wire Wire Line
	4250 3650 4150 3650
Wire Wire Line
	4150 3650 4150 4200
Wire Wire Line
	4150 4800 5000 4800
Connection ~ 5000 4800
Wire Wire Line
	4250 4200 4150 4200
Connection ~ 4150 4200
Wire Wire Line
	4150 4200 4150 4800
Wire Wire Line
	5400 4800 6500 4800
Connection ~ 6500 4800
$EndSCHEMATC
