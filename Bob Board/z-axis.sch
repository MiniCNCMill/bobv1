EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 6950 3800 0    50   Input ~ 0
SDI
Text HLabel 6950 3900 0    50   Input ~ 0
SDO
Text HLabel 6950 3700 0    50   Input ~ 0
SCK
$Comp
L TMC:TMC5160_BOB U?
U 1 1 61059B3B
P 7850 4200
AR Path="/60FF5A28/61059B3B" Ref="U?"  Part="1" 
AR Path="/60FF6D3F/61059B3B" Ref="U3"  Part="1" 
F 0 "U3" H 7850 5181 50  0000 C CNN
F 1 "TMC5160_BOB" H 7850 5090 50  0000 C CNB
F 2 "TMC:DIP-22_1400_ELL" H 6750 5900 50  0001 C CNN
F 3 "" H 6750 5900 50  0001 C CNN
	1    7850 4200
	1    0    0    -1  
$EndComp
Text HLabel 6950 3600 0    50   Input ~ 0
CSN
Wire Wire Line
	6950 3600 7150 3600
Wire Wire Line
	6950 3700 7150 3700
Wire Wire Line
	6950 3800 7150 3800
Wire Wire Line
	6950 3900 7150 3900
$Comp
L power:GND #PWR?
U 1 1 61059B46
P 7650 5000
AR Path="/61059B46" Ref="#PWR?"  Part="1" 
AR Path="/60FF5A28/61059B46" Ref="#PWR?"  Part="1" 
AR Path="/60FF6D3F/61059B46" Ref="#PWR05"  Part="1" 
F 0 "#PWR05" H 7650 4750 50  0001 C CNN
F 1 "GND" H 7655 4827 50  0000 C CNN
F 2 "" H 7650 5000 50  0001 C CNN
F 3 "" H 7650 5000 50  0001 C CNN
	1    7650 5000
	1    0    0    -1  
$EndComp
$Comp
L power:Vdrive #PWR?
U 1 1 61059B4C
P 8050 3100
AR Path="/61059B4C" Ref="#PWR?"  Part="1" 
AR Path="/60FF5A28/61059B4C" Ref="#PWR?"  Part="1" 
AR Path="/60FF6D3F/61059B4C" Ref="#PWR06"  Part="1" 
F 0 "#PWR06" H 7850 2950 50  0001 C CNN
F 1 "Vdrive" H 8065 3273 50  0000 C CNN
F 2 "" H 8050 3100 50  0001 C CNN
F 3 "" H 8050 3100 50  0001 C CNN
	1    8050 3100
	1    0    0    -1  
$EndComp
$Comp
L power:+5P #PWR?
U 1 1 61059B52
P 7650 3100
AR Path="/61059B52" Ref="#PWR?"  Part="1" 
AR Path="/60FF5A28/61059B52" Ref="#PWR?"  Part="1" 
AR Path="/60FF6D3F/61059B52" Ref="#PWR04"  Part="1" 
F 0 "#PWR04" H 7650 2950 50  0001 C CNN
F 1 "+5P" H 7665 3273 50  0000 C CNN
F 2 "" H 7650 3100 50  0001 C CNN
F 3 "" H 7650 3100 50  0001 C CNN
	1    7650 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 5000 7950 5000
Connection ~ 7650 5000
Connection ~ 7750 5000
Wire Wire Line
	7750 5000 7650 5000
Connection ~ 7950 5000
Wire Wire Line
	7950 5000 7750 5000
Wire Wire Line
	7650 3400 7650 3100
Wire Wire Line
	8050 3100 8050 3400
$Comp
L Connector:Screw_Terminal_01x04 J?
U 1 1 61059B60
P 8750 3700
AR Path="/60FF5A28/61059B60" Ref="J?"  Part="1" 
AR Path="/60FF6D3F/61059B60" Ref="J4"  Part="1" 
F 0 "J4" H 8830 3692 50  0000 L CNN
F 1 "Screw_Terminal_01x04" H 8830 3601 50  0000 L CNN
F 2 "TerminalBlock:TerminalBlock_bornier-4_P5.08mm" H 8750 3700 50  0001 C CNN
F 3 "~" H 8750 3700 50  0001 C CNN
	1    8750 3700
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J?
U 1 1 610623E6
P 3450 3750
AR Path="/60FF5A28/610623E6" Ref="J?"  Part="1" 
AR Path="/60FF6D3F/610623E6" Ref="J7"  Part="1" 
F 0 "J7" H 3368 3425 50  0000 C CNN
F 1 "Screw_Terminal_01x02" H 3368 3516 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 3450 3750 50  0001 C CNN
F 3 "~" H 3450 3750 50  0001 C CNN
	1    3450 3750
	-1   0    0    1   
$EndComp
Text HLabel 6850 4200 0    50   Input ~ 0
DRV_ENN
Wire Wire Line
	6850 4200 7150 4200
Wire Wire Line
	7150 4100 7050 4100
Wire Wire Line
	7050 4100 7050 5000
Wire Wire Line
	7050 5000 7650 5000
Text HLabel 8950 4800 2    50   Output ~ 0
DIAG
Wire Wire Line
	8550 4800 8950 4800
$Comp
L Isolator:PC827 U?
U 1 1 610AE75C
P 5100 3750
AR Path="/60FF5A28/610AE75C" Ref="U?"  Part="1" 
AR Path="/60FF6D3F/610AE75C" Ref="U6"  Part="1" 
F 0 "U6" H 5100 4075 50  0000 C CNN
F 1 "PC827" H 5100 3984 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 4900 3550 50  0001 L CIN
F 3 "http://optoelectronics.liteon.com/upload/download/DS-70-96-0016/LTV-8X7%20series.PDF" H 5100 3750 50  0001 L CNN
	1    5100 3750
	1    0    0    -1  
$EndComp
$Comp
L Isolator:PC827 U?
U 2 1 610AE762
P 5100 4300
AR Path="/60FF5A28/610AE762" Ref="U?"  Part="2" 
AR Path="/60FF6D3F/610AE762" Ref="U6"  Part="2" 
F 0 "U6" H 5100 4625 50  0000 C CNN
F 1 "PC827" H 5100 4534 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 4900 4100 50  0001 L CIN
F 3 "http://optoelectronics.liteon.com/upload/download/DS-70-96-0016/LTV-8X7%20series.PDF" H 5100 4300 50  0001 L CNN
	2    5100 4300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 610AE768
P 5550 4850
AR Path="/60FF5A28/610AE768" Ref="R?"  Part="1" 
AR Path="/60FF6D3F/610AE768" Ref="R12"  Part="1" 
F 0 "R12" H 5620 4896 50  0000 L CNN
F 1 "R" H 5620 4805 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Vertical" V 5480 4850 50  0001 C CNN
F 3 "~" H 5550 4850 50  0001 C CNN
	1    5550 4850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 610AE76E
P 5950 4850
AR Path="/60FF5A28/610AE76E" Ref="R?"  Part="1" 
AR Path="/60FF6D3F/610AE76E" Ref="R13"  Part="1" 
F 0 "R13" H 6020 4896 50  0000 L CNN
F 1 "R" H 6020 4805 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Vertical" V 5880 4850 50  0001 C CNN
F 3 "~" H 5950 4850 50  0001 C CNN
	1    5950 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 4300 6100 4300
Wire Wire Line
	6100 4300 6100 3850
$Comp
L power:+5V #PWR?
U 1 1 610AE776
P 5450 3550
AR Path="/60FF5A28/610AE776" Ref="#PWR?"  Part="1" 
AR Path="/60FF6D3F/610AE776" Ref="#PWR0116"  Part="1" 
F 0 "#PWR0116" H 5450 3400 50  0001 C CNN
F 1 "+5V" H 5465 3723 50  0000 C CNN
F 2 "" H 5450 3550 50  0001 C CNN
F 3 "" H 5450 3550 50  0001 C CNN
	1    5450 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 3550 5450 3650
Wire Wire Line
	5450 4200 5400 4200
Wire Wire Line
	5400 3650 5450 3650
Connection ~ 5450 3650
Wire Wire Line
	5450 3650 5450 4200
Wire Wire Line
	5400 4400 5950 4400
Wire Wire Line
	5400 3850 5550 3850
Wire Wire Line
	5550 5000 5950 5000
Connection ~ 5950 5000
Wire Wire Line
	5550 4700 5550 3850
Connection ~ 5550 3850
Wire Wire Line
	5550 3850 6100 3850
Wire Wire Line
	5950 4700 5950 4400
Connection ~ 5950 4400
Wire Wire Line
	5950 4400 7150 4400
$Comp
L Device:R R?
U 1 1 610AE78C
P 4400 3650
AR Path="/60FF5A28/610AE78C" Ref="R?"  Part="1" 
AR Path="/60FF6D3F/610AE78C" Ref="R10"  Part="1" 
F 0 "R10" V 4193 3650 50  0000 C CNN
F 1 "R" V 4284 3650 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Vertical" V 4330 3650 50  0001 C CNN
F 3 "~" H 4400 3650 50  0001 C CNN
	1    4400 3650
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 610AE792
P 4400 4200
AR Path="/60FF5A28/610AE792" Ref="R?"  Part="1" 
AR Path="/60FF6D3F/610AE792" Ref="R11"  Part="1" 
F 0 "R11" V 4193 4200 50  0000 C CNN
F 1 "R" V 4284 4200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Vertical" V 4330 4200 50  0001 C CNN
F 3 "~" H 4400 4200 50  0001 C CNN
	1    4400 4200
	0    1    1    0   
$EndComp
Wire Wire Line
	3650 3650 4250 3650
Wire Wire Line
	4550 3650 4800 3650
Wire Wire Line
	3650 3750 4100 3750
Wire Wire Line
	4100 3750 4100 4200
Wire Wire Line
	4100 4200 4250 4200
Wire Wire Line
	4550 4200 4800 4200
Wire Wire Line
	4800 3850 4700 3850
Wire Wire Line
	4700 3850 4700 4400
Wire Wire Line
	4700 5000 5550 5000
Connection ~ 5550 5000
Wire Wire Line
	4800 4400 4700 4400
Connection ~ 4700 4400
Wire Wire Line
	4700 4400 4700 5000
Wire Wire Line
	5950 5000 7050 5000
Connection ~ 7050 5000
$EndSCHEMATC
